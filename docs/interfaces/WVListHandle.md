# Interface: WVListHandle

Methods of [WVList](../API.md#wvlist).

## Table of contents

### Properties

- [cache](WVListHandle.md#cache)

## Properties

### cache

• `Readonly` **cache**: [`CacheSnapshot`](CacheSnapshot.md)

Get current [CacheSnapshot](CacheSnapshot.md).

#### Defined in

[src/react/WVList.tsx:45](https://github.com/inokawa/virtua/blob/95a48eb8/src/react/WVList.tsx#L45)
