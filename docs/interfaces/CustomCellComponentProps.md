# Interface: CustomCellComponentProps

Props of customized cell component for [VGrid](../API.md#vgrid).

## Table of contents

### Properties

- [style](CustomCellComponentProps.md#style)
- [children](CustomCellComponentProps.md#children)

## Properties

### style

• **style**: `CSSProperties`

#### Defined in

[src/react/VGrid.tsx:43](https://github.com/inokawa/virtua/blob/95a48eb8/src/react/VGrid.tsx#L43)

___

### children

• **children**: `ReactNode`

#### Defined in

[src/react/VGrid.tsx:44](https://github.com/inokawa/virtua/blob/95a48eb8/src/react/VGrid.tsx#L44)
